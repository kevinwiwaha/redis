<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Blog;
use COM;

class BlogController extends Controller
{
    public function index()
    {
        $data = Cache::remember('blogs', 5, function () {
            return Blog::all();
        });

        return view('blog', ['blog' => $data]);
    }
}
